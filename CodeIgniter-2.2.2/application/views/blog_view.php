<html>
	<head>
		<title><?=$title ?></title>
	</head>

	<body>

		<h1><?=$heading?></h1>
		<p><?='<h2>Hi '. $this->session->userdata('session_array')['username'].'!</h2>' ?>
		<?=anchor('blog/register', 'Register')?>
		<?=anchor('blog/logout', 'Logout')?></p>
		<hr />
		
		<p><?=anchor('blog/new_entry', 'New Entry')?></p>
		<?php foreach($query->result() as $row): ?>
			<h3><?=$row->title?></h3>
			<p><?=nl2br($row->body)?></p>
			<?=anchor('blog/edit_entry/'.$row->id,'Edit')?>
			<?=anchor('blog/delete_entry/'.$row->id,'Delete')?></p>
			<hr />
		<?php endforeach; ?>
		
	</body>
</html>