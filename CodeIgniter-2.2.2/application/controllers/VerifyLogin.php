<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class VerifyLogin extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
 }
 
 /**
 * Start of backend of login process
 * 
 * Validates form. If invalid, return back to form. If valid, go to private area. One part of the validation includes attempting to log in.
 */
 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');
 
   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
 
   if($this->form_validation->run() == FALSE)
	{
		//Field validation failed.  User redirected to login page
		$data['title'] = "Matthew's Shopback Blog Title (Login)";
		$data['heading'] = "Matthew's Shopback Blog Heading (Login)";
		$this->load->view('login_view', $data);
	}
	else
	{
		//Go to private area
		redirect('blog', 'refresh');
	}
 
 }
	/**
	* checks the database for the spcified user and sets session data
	* 
	* checks the database thourgh the user model whether the user has entered correct credentials, then sets the session with the id and username of the user if correct. else return user to login page with error message.
	* 
	* @return True on successful login attempt, or false if otherwise
	*/
	function check_database()
	{
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		//query the database
		$result = $this->user->login($username, $password);

		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{
				$sess_array = array(
					'id' => $row->id,
					'username' => $row->username
				);
				$this->session->set_userdata('session_array', $sess_array);
			}
				return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}
}
?>