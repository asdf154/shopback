<?php

class Blog extends CI_Controller {
	function index(){
		$this->load->helper('url');
		$this->load->helper('form');
		
		$data['title'] = "Matthew's Shopback Blog Title";
		$data['heading'] = "Matthew's Shopback Blog Heading";
		$data['query'] = $this->db->get('entries');
		
		$this->load->view('blog_view', $data);
	}
	
	function comments(){
		echo 'at';
	}
}

?>