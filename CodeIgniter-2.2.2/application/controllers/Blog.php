<?php

class Blog extends CI_Controller {
	/**
	* Constructor which loads the Blog_Model model and the User model.
	*/
	function Blog(){
		parent::__construct();
		$this->load->model('Blog_Model');
		$this->load->model('User');
	}
	
	/**
	* Checks if user is logged in.
	* 
	* Checks if user is logged in. If not, redirect user to login view. If yes, do nothing. I.e. let the caller carry on.
	* 
	* $return Returns false on invalid login
	*/
	function check_logged_in(){
		$session_array = $this->session->userdata('session_array');
		//print_r($session_array);
		
		if(is_null($session_array) || is_null($session_array['username'])){
			$data['title'] = "Matthew's Shopback Blog Title (Login)";
			$data['heading'] = "Matthew's Shopback Blog Heading (Login)";
			redirect('Blog/show_login', $data);
			return false;
		}
		return true;
	}
	
	function show_login(){
		$data['title'] = "Matthew's Shopback Blog Title (Login)";
		$data['heading'] = "Matthew's Shopback Blog Heading (Login)";
		$this->load->view('login_view', $data);
	}

	/*
	* Shows the blog entries
	* 
	* 
	* 
	* 
	* 
	*/
	function blog_view($id = null){
		if ($id == null){
			$this->check_logged_in();
			$data['title'] = "Matthew's Shopback Blog Title";
			$data['heading'] = "Matthew's Shopback Blog Heading";
			$data['Blog'] = $this->blogModel->get_all_entries();
			$this->load->view('blog_view', $data);
		} else{
			//TODO: show only that post
		}
	}
	
 /**
 * Standard entry point in to the site
 * 
 * All users should start here when entering site. Here they either get directed to the login view, or to the logged in area.
 * 
 */
	function index(){
		// checks whether user is logged in
		$this->check_logged_in();
		//yes, user is logged in, so load the blog_view view
			$session_data = $this->session->userdata('logged_in');
			$data['title'] = "Matthew's Shopback Blog Title";
			$data['heading'] = "Matthew's Shopback Blog Heading";
			$data['username'] = $session_data['username'];
			$data['query'] = $this->db->get('entries'); //TODO: 
			$this->load->view('blog_view', $data);
		
	}
	
	/**
	*/
	function register(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$data['title'] = "Matthew's Shopback Blog Title (register)";
		$data['heading'] = "register";

		//TODO: validations
		$this->form_validation->set_rules('username', 'username', 'required|trim|max_length[20]|xss_clean|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'password', 'required|trim|max_length[20]|xss_clean');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('register', $data);
		}
		else
		{
			$password = $this->input->post('password');
			$username = $this->input->post('username');
			$this->User->register($username, $password);
			redirect(Blog/index); //TODO: replace with a view of the particular entry
		}
	}

	/*
	* Logs the user out by destroying the session and then refreshing
	* 
	*/
	function logout()
	{
		$this->session->unset_userdata('session_array');
		session_destroy();
		redirect('blog', 'refresh');
	}
	
	/*
	* Attempt to create a new blog entry after validation.
	* 
	* Validates the form as sent through POST. If invalid, return back to form. If valid, creates a new blog entry through the Blog_Model model.
	* 
	*/
	function new_entry(){
		$this->check_logged_in();
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$data['title'] = "Matthew's Shopback Blog Title (New Entry)";
		$data['heading'] = "New Entry";

		//TODO: validations
		$this->form_validation->set_rules('title', 'Title', 'required|trim|');
		$this->form_validation->set_rules('body', 'Body', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('new_entry', $data);
		}
		else
		{
			$this->Blog_Model->new_entry();
			redirect(Blog/index); //TODO: replace with a view of the particular entry
		}
	}
	
	/*
	* Attempts to delete a blog entry.
	* 
	* Deletes the blog entry with the ID as specified in the third segment of the url.
	* 
	*/
	function delete_entry(){
		$this->check_logged_in();
		$this->Blog_Model->delete_entry($this->uri->segment(3));
		redirect(Blog/index); //TODO: replace with a view of the particular entry
	}
	
	/*
	* Edits a particular blog entry.
	* 
	* Checks if POST data is empty. If empty, load the edit view.
	* If POST data is present, attempts to update the database though the Blog_Model model.
	*/
	function edit_entry(){
		if(empty($this->input->post())){
			$data['title'] = "Matthew's Shopback Blog Title (Edit Entry)";
			$data['heading'] = "Edit Entry";
			$this->load->view('blog_edit', $data);
		} else{
			$this->Blog_Model->edit_entry($this->uri->segment(3));
			redirect(Blog/index); //TODO: replace with a view of the particular entry
		}
	}
}

?>