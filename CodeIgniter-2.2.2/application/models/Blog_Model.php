<?php

class Blog_Model extends CI_Model{
	/**
	* Constructor. Loads the database helper/library of codeigniter.
	*/
	public function __construct()
	{
		$this->load->database();
	}
	
	/**
	* gets all blog entries in the database.
	*/
	public function get_all_entries(){
		return $this->db->get('entries');
	}
	
	/**
	* Delete a blog entry.
	* 
	* @param $id the id of the blog post to be deleted.
	*/
	public function delete_entry($id){
		//$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		return $this->db->delete('entries');
	}
	
	/**
	* Edits specified blog entry
	* 
	* Edits the blog entry with the id, title and body as specified in POST data.
	*
	* @param $id is the id of the blog post to be edited
	*/
	public function edit_entry($id){
	echo $id;
		$data = array(
			'title' => $this->input->post('title'),
			'body' => $this->input->post('body'),
			'id' => $id
		);

		print_r($data);
		$this->db->where('id', $id);
		return $this->db->update('entries', $data);
	}
	
	/**
	* Creates a new blog entry.
	*
	* Takes title and body from POST data and then inserts it into database.
	*/
	public function new_entry(){
		$data = array(
			'title' => $this->input->post('title'),
			'body' => $this->input->post('body'),
		);
		
		return $this->db->insert('entries', $data);
	}
}

?>