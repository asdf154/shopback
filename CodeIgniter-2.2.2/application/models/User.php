<?php

	class User extends CI_Model{
	/**
	* Checks database and returns a row if it exists
	* 
	* Checks database where the username and password are the same as those passed in to the arguments
	* 
	* @param string $username Username of the user attempting to login
	* @param string $password Password of the user attempting to login
	* 
	* @return $query->result(). i.e. The query result as an array of objects, or an false on failure
	*/
	function login($username, $password)
	{
		$this -> db -> select('id, username, password');
		$this -> db -> from('users');
		$this -> db -> where('username', $username);
		$this -> db -> where('password', $password);
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	/**
	* Registers a new user with the specified username and password
	
	* @param string $username Username of the user attempting to register
	* @param string $password Password of the user attempting to register
	*/
	function register($username, $password){
		$data = array(
			'username' => $username,
			'password' => $password
		);
		
		$this->db->insert('users', $data);
	}
}

?>